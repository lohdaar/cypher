```
[  OK  ] Started Show Plymouth Boot Screen.
[  OK  ] Reached target Paths.
[  OK  ] Reached target Basic System.
[  OK  ] Found device ARKHA-UPSILON.
[  OK  ] Started dracut initqueue hook.
         Starting dracut pre-mount hook...
[  OK  ] Reached target Remote File Systems (Pre).
[  OK  ] Reached target Remote File Systems.
[  OK  ] Started dracut pre-mount hook.
         Starting File System Check on /dev/disk/by-uuid/85e4ae33-c60e-4372-a6ba-9aeb23bf6d86...
[  OK  ] Started File System Check on /dev/disk/by-uuid/85e4ae33-c60e-4372-a6ba-9aeb23bf6d86.
         Mounting /sysroot...
[  OK  ] Mounted /sysroot.
[  OK  ] Reached target Initrd Root File System.
         Starting Reload Configuration from the Real Root...
[  OK  ] Started Reload Configuration from the Real Root.
[  OK  ] Reached target Initrd File Systems.
[  OK  ] Reached target Initrd Default Target.

[ WARNING ] Corrupted file system detected. You may experience data or memory leaks.

Welcome to ArkhOS!

Starting Collect Read-Ahead Data...
[  OK  ] Created slice User and Session Slice.
[  OK  ] Created slice system-getty.slice.
[  OK  ] Reached target Remote File Systems (Pre).
[  OK  ] Reached target Remote File Systems.
[  OK  ] Reached target System Time Synchronized.
[  OK  ] Reached target Slices.
[  OK  ] Listening on Delayed Shutdown Socket.
[  OK  ] Listening on /dev/initctl Compatibility Named Pipe.
         Starting Rule generator for /dev/root symlink...
[  OK  ] Stopped Trigger Flushing of Journal to Persistent Storage.
         Stopping Journal Service...
[  OK  ] Stopped Journal Service.
         Starting Journal Service...
[  OK  ] Reached target Paths.
         Mounting Debug File System...

[ WARNING ] Anomalies detected in cypher.md file.
            Executing file ...

[  OK  ] Accessing FR locales.
         French locales initialiazed.

```

# ~~**[̲̅C][̲̅Y][̲̅P][̲̅H][̲̅E][̲̅R]**~~

---
> **[ DONNÉES CORROMPUES DÉTECTÉES - CONSULTEZ CE REGISTRE AVEC PRUDENCE ]**

---

#### **[ - NOM - ]**

```
[  OK  ] Lancement du script de recherche identitaire.
[ ERREUR ] Les données liées à l'identité du sujet sont corrompues.
[ ERREUR ] Identité inconnue.
         ROLLBACK
         Recherche d'alias en cours...

1 alias trouvé. "Cypher"
```

---

#### **[ - AGE - ]**

```
[  OK  ] Lancement du script d'historisation biométrique.
         Recherche d'empreinte génétique...
         Recherche d'extrait ADN dans la base de données...
         Aucune entrée.
[ ERREUR ] Impossible de défnir la longévité de l'individu.

Longévité de "Cypher" inconnue.
```

---

#### **[ - PHYSIQUE - ]**

```
[  OK  ] Lancement du script d'identification.
         Initialisation des paramètres vectoriels...
         Recherche dans la banque d'imagerie...
         Lecture des légendes...
         Affichage de 2 resultats.
```

![](https://data.whicdn.com/images/200675850/large.jpg)
> **Cypher** durant l'opposition.

---

![](https://orig00.deviantart.net/e560/f/2015/216/2/5/silhouette_by_reza_ilyasa-d945iqt.jpg)
> **Cypher** tel qu'il aurait été aperçu le **~~[DONNÉES CORROMPUES]~~**.


##### Attitude du sujet :

```
[  OK  ] Lancement du script d'analyse des profils comportementaux.
         Lecture de la fiche comportementale 32481...
         Affichage de la fiche 32481.
```

> **Cypher**, pour le peu de fois qu'il aurait été aperçu, ne présente aucune particuliarité physique des temps de nos jours. Tout porte à croire que le sujet est un humain âgé de 20 à 30 ans (à déterminer), mesurant 1m75, aux cheveux bruns et le teint pâle. 
Aucune autre information sur son physique n'a jusqu'alors été enregistrée.

---

#### **[ - SPÉCIALITÉ - ]**

> **Cypher** fait l'objet d'accusations graves pour de multiples fraudes informatiques allant à l'encontre de **[CONFIDENTIEL]**. Le sujet ferait aussi partie de la société **~~[DONNÉES CORROMPUES]~~**.

![](https://i.imgur.com/Y4YLkuv.png)
> Iconographie de la société **~~[DONNÉES CORROMPUES]~~**.

---

```
Fin du registre.
```

